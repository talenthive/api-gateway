// jest.config.js
module.exports = {
    rootDir: '',
    roots: ['<rootDir>/src', '<rootDir>/types'],
    globals: {
        fetch: global.fetch,
    },
    moduleFileExtensions: ['js', 'json', 'ts'],
    modulePaths: ['<rootDir>/src/modules.'],
    setupFiles: [
        '<rootDir>/test/dotenv-config.js',
        '<rootDir>/test/setup-file.ts',
    ],
    globalSetup: '<rootDir>/test/global-setup.ts',
    globalTeardown: '<rootDir>/test/global-teardown.ts',
    setupFilesAfterEnv: ['<rootDir>/test/setup-file-after-env.ts'],
    moduleNameMapper: {
        '^src/(.*)$': '<rootDir>/src/$1',
        '@/(.*)$': '<rootDir>/src/$1',
        '@Application/(.*)$': '<rootDir>/src/application/$1',
        '@Types': '<rootDir>/types/$1',
        '@Prisma/(.*)$': '<rootDir>/src/infrastructure/prisma/$1',
        '@Functions/(.*)$': '<rootDir>/src/application/functions/$1',
        '@Enums/(.*)$': '<rootDir>/src/application/enums/$1',
        '@Modules/(.*)$': '<rootDir>/src/modules/$1',
        '@Api/(.*)$': '<rootDir>/src/api/$1',
        '@Infrastructure/(.*)$': '<rootDir>/src/infrastructure/$1',
        '@Schemas/(.*)$': '<rootDir>/prisma/schema/$1',
        '@Application/(.*)$': '<rootDir>/src/application/$1',
        '@Entities/(.*)$': '<rootDir>/src/entities/$1',
        '@Helpers/(.*)$': '<rootDir>/src/application/helpers/$1',
        '@Decorators/(.*)$': '<rootDir>/src/application/decorators/$1',
        '@Tools/(.*)$': '<rootDir>/src/application/tools/$1',
    },
    testRegex: '.*\\.(test|spec)\\.ts$',
    preset: 'ts-jest',
    transform: {
        '^.+\\.tsx?$': [
            'ts-jest',
            {
                tsconfig: 'tsconfig.json',
            },
        ],
    },
    collectCoverageFrom: ['**/*.(t|j)s'],
    coverageDirectory: '../coverage',
    testEnvironment: 'node',
};
