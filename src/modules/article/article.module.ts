import { Module } from '@nestjs/common';
import { ArticleController } from './article.controller';
import { ArticleService } from './article.service';

//Modules
import { ElasticSearchModule } from '@/infrastructure/elastic-search/elastic-search.module';
import { RedisModule } from '@/infrastructure/redis/redis.module';

@Module({
    imports: [ElasticSearchModule, RedisModule],
    controllers: [ArticleController],
    providers: [ArticleService]
})
export class ArticleModule {}
