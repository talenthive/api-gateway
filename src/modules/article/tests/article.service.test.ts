import { Test, TestingModule } from '@nestjs/testing';

import { createFakeArticle } from '@Infrastructure/faker/article.faker';
import { ArticleService } from '../article.service';
import { ArticleCreateInput } from '../dto/article.create.input';

import { ElasticSearchModule } from '@/infrastructure/elastic-search/elastic-search.module';
import { PrismaService } from '@/infrastructure/prisma/prisma.service';
import { RedisModule } from '@/infrastructure/redis/redis.module';
import { ConfigModule } from '@nestjs/config';

describe('Article Service', () => {
    let articleService: ArticleService;
    let article: ArticleCreateInput;
    let articleId: string;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ElasticSearchModule,
                RedisModule,
                ConfigModule.forRoot({
                    isGlobal: true,
                }),
            ],
            controllers: [],
            providers: [ArticleService, PrismaService],
        }).compile();

        articleService = module.get<ArticleService>(ArticleService);
    });

    //Service
    it('Article Service should be defined', () => {
        expect(articleService).toBeDefined();
    });

    //Generate fake article data
    it('Should generate a fake article', async () => {
        article = createFakeArticle();
        expect(article).toBeDefined();
        expect(Object.keys(article).length).toBeGreaterThan(0);
    });

    // //Create Article
    it('Should create a article', async () => {
        const newArticle = await articleService.create(article);
        articleId = newArticle.id;
        expect(Object.keys(newArticle).length).toBeGreaterThan(0);
    });

    // //Update Article
    // it('Should update a article', async () => {
    //     const fakeArticle = createFakeArticle();
    //     const updatedArticle = { ...fakeArticle, id: articleId };
    //     const article = await articleService.update(articleId, updatedArticle);
    //     expect(Object.keys(article).length).toBeGreaterThan(0);
    // });

    // //Find Articles
    // it('Should return articles', async () => {
    //     const articles = await articleService.find({});
    //     expect(articles?.length).toBeGreaterThan(0);
    // });

    // //Delete Article
    // it('Should delete the article', async () => {
    //     const result = await articleService.delete(articleId);
    //     expect(result).toBeTruthy();
    // });
});
