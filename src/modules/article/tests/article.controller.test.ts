import { Test } from '@nestjs/testing';
import { ArticleController } from '../article.controller';
import { ArticleService } from '../article.service';
import { PrismaService } from '@/infrastructure/prisma/prisma.service';
import { RedisModule } from '@/infrastructure/redis/redis.module';
import { ElasticSearchModule } from '@/infrastructure/elastic-search/elastic-search.module';
import { ConfigModule } from '@nestjs/config';

describe('ArticleController', () => {
    let articleController: ArticleController;

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                ElasticSearchModule,
                RedisModule,
                ConfigModule.forRoot({
                    isGlobal: true,
                }),
            ],
            controllers: [ArticleController],
            providers: [ArticleService, PrismaService],
        }).compile();

        articleController = moduleRef.get<ArticleController>(ArticleController);
    });

    it('Article Controller Should be defined', () => {
        expect(articleController).toBeDefined();
    });
});
