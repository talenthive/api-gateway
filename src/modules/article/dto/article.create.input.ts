import { CreateDTO } from '@/application/dtos/create.dto';
import { Prisma } from '@prisma/client';
import { IsString } from 'class-validator';

export class ArticleCreateInput
    extends CreateDTO
    implements Prisma.ArticleCreateInput
{
    @IsString()
    title: string;

    @IsString()
    content: string;
}
