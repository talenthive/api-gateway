import { PartialType } from '@nestjs/mapped-types';
import { ArticleCreateInput } from './article.create.input';

export class ArticleUpdateInput extends PartialType(ArticleCreateInput) {}
