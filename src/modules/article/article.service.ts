import { Injectable } from '@nestjs/common';

//Prisma
import { Article, Prisma } from '@prisma/client';

//Modules

//Services
import { GenericService } from '@/application/services/generic.service';
import { ElasticSearchService } from '@/infrastructure/elastic-search/elastic-search.service';
import { PrismaService } from '@/infrastructure/prisma/prisma.service';

@Injectable()
export class ArticleService extends GenericService<
    Article,
    Prisma.ArticleDelegate
> {
    constructor(
        protected readonly prismaService: PrismaService,
        protected readonly elasticSearchService: ElasticSearchService
    ) {
        super('article', prismaService, elasticSearchService);
    }
}
