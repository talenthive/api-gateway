import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Query,
} from '@nestjs/common';

//Article
import { Article } from '@prisma/client';
import { ArticleService } from './article.service';
import { ArticleArgs } from './dto/article.find.input';

//DTO
import { SearchQueryDTO } from '@/application/dtos/search.dto';
import { ArticleCreateInput } from './dto/article.create.input';

@Controller('article')
export class ArticleController {
    constructor(private readonly articleService: ArticleService) {}

    //Create Article
    @Post('/create')
    async create(@Body() body: ArticleCreateInput): Promise<Article> {
        // this.logger.log('Create Article');
        return await this.articleService.create(body);
    }

    //Get Article
    @Get('/find/:id')
    async findOne(@Param('id') id: string): Promise<Article> {
        return await this.articleService.findById(id);
    }

    //Get Articles
    @Get('/find')
    async find(@Body() body: ArticleArgs): Promise<Article[]> {
        return await this.articleService.find(body);
    }

    //Search Articles
    @Get('/search')
    async search(@Query() body: SearchQueryDTO): Promise<Article[]> {
        const { text } = body;
        return await this.articleService.search(text, ['title', 'content']);
    }

    //Remove Article
    @Delete('/remove/:id')
    async remove(@Param('id') id: string): Promise<Boolean> {
        return await this.articleService.delete(id);
    }
}
