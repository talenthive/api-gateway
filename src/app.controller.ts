import { Controller, Get, Logger } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthNotRequired } from './application/guards/auth.guard';

@Controller()
export class AppController {
    constructor(
        private readonly appService: AppService,
        private readonly logger: Logger
    ) {}

    @Get()
    @AuthNotRequired(true)
    async getHello(): Promise<string> {
        return 'Hello World !';
    }
}
