import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

//Logger
import { WinstonModule } from 'nest-winston';
import { winstonConfig } from './infrastructure/winston/winston.config';

const port = process.env.PORT || 3000;

async function bootstrap() {
    const app = await NestFactory.create(AppModule, {
        logger: WinstonModule.createLogger(winstonConfig),
    });

    //Cors
    app.enableCors();

    //Validation Pipes
    app.useGlobalPipes(
        new ValidationPipe({
            transform: true,
        })
    );

    await app.listen(port);
}
bootstrap()
    .then(() => Logger.log(`Nest Is Ready on port ${port}`))
    .then(() => Logger.log(`Environment is: ${process.env.NODE_ENV}`))
    .catch((err) => Logger.error('Nest broken', err));
