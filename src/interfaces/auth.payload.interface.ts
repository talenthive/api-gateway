export interface AuthPayload {
    appId: number;
    userId: number;
}
