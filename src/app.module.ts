import { Logger, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { AppController } from './app.controller';
import { AppService } from './app.service';

//Interceptors
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import ResponseInterceptor from './application/interceptors/response.interceptor';

//Filters
import { AllExceptionsFilter } from './application/exceptions/all.exceptions';

//Infra Modules
import { ElasticSearchModule } from './infrastructure/elastic-search/elastic-search.module';
import { LambdaModule } from './infrastructure/lambda/lambda.module';
import { RedisModule } from './infrastructure/redis/redis.module';

//Business Modules
import { ArticleModule } from '@Modules/article/article.module';

//Guards
import { CompanyFilterGuard } from './application/filters/company.filter';
import { AuthJWTGuard } from './application/guards/auth.guard';
import { CompanyGuard } from './application/guards/company.guard';
import { OnlyMeGuard } from './application/guards/onlyMe.guard';
import { RoleGuard } from './application/guards/role.guard';
import { PrismaModule } from './infrastructure/prisma/prisma.module';
@Module({
    imports: [
        LambdaModule,
        ElasticSearchModule,
        RedisModule,
        ConfigModule.forRoot({
            isGlobal: true,
        }),
        PrismaModule,
        //JWT
        JwtModule.registerAsync({
            useFactory: async (configService: ConfigService) => ({
                secret: process.env.JWT_SECRET,
                signOptions: { expiresIn: '12h' },
            }),
            inject: [ConfigService],
        }),
        //Business Modules
        ArticleModule,
    ],
    controllers: [AppController],
    providers: [
        Logger,
        AppService,
        {
            provide: APP_INTERCEPTOR,
            useClass: ResponseInterceptor,
        },
        {
            provide: APP_FILTER,
            useClass: AllExceptionsFilter,
        },
        {
            provide: APP_GUARD,
            useClass: AuthJWTGuard,
        },
        {
            provide: APP_GUARD,
            useClass: CompanyGuard,
        },
        {
            provide: APP_GUARD,
            useClass: RoleGuard,
        },
        {
            provide: APP_GUARD,
            useClass: OnlyMeGuard,
        },
        {
            provide: APP_GUARD,
            useClass: CompanyFilterGuard,
        },
    ],
})
export class AppModule {}
