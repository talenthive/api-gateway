import { Transform } from 'class-transformer';
import { IsBoolean, IsNumber } from 'class-validator';

export class RemoveDTO {
    @IsNumber()
    id: number;

    @IsBoolean()
    @Transform((value) => (value ? value : true))
    softDelete?: boolean;
}
