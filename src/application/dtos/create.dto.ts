import { IsDate, IsOptional } from 'class-validator';

export class CreateDTO {
    @IsDate()
    @IsOptional()
    createdAt?: Date | string;

    @IsDate()
    @IsOptional()
    updatedAt?: Date | string;

    @IsDate()
    @IsOptional()
    deletedAt?: Date | string;
}
