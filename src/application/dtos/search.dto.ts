import { IsDate, IsOptional, IsString } from 'class-validator';

export class SearchQueryDTO {
    @IsString()
    text: string;
}
