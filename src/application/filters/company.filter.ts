import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

import { SetMetadata } from '@nestjs/common';

export const CompanyFilter = (...args: any[]) =>
    SetMetadata('CompanyFilter', args);

@Injectable()
export class CompanyFilterGuard extends AuthGuard('jwt') {
    constructor(private reflector: Reflector) {
        super();
    }
    async canActivate(context: ExecutionContext): Promise<any> {
        const restriction: boolean = this.reflector.get(
            'CompanyFilter',
            context.getHandler()
        );

        //No Restriction
        if (!restriction) return true;

        const request = context.switchToHttp().getRequest();
        const user = request.user;
        const companies = user.companies;

        //Edit Body
        Object.assign(request.body, {
            companyId: companies.map((c) => c.id),
        });

        return true;
    }
}
