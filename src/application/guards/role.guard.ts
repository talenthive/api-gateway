import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

import { SetMetadata } from '@nestjs/common';

export const CheckRole = (...args: any[]) => SetMetadata('CheckRole', args);

@Injectable()
export class RoleGuard extends AuthGuard('jwt') {
    constructor(private reflector: Reflector) {
        super();
    }
    async canActivate(context: ExecutionContext): Promise<any> {
        const restriction: boolean = this.reflector.get(
            'CheckRole',
            context.getHandler()
        );

        //No Restriction
        if (!restriction) return true;

        const request = context.switchToHttp().getRequest();
        const params = request.params;
        const roleId = params.roleId;
        const user = request.user;
        const roles = user.roles;
        const superAdmin = request.superAdmin;

        //User is a super admin
        if (superAdmin) return true;

        //Exclusion
        if (!roles.includes(roleId)) return false;
        else return true;
    }
}
