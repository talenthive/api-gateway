import { LambdaService } from '@/infrastructure/lambda/lambda.service';
import {
    CanActivate,
    ExecutionContext,
    HttpException,
    HttpStatus,
    Injectable,
    UnauthorizedException,
} from '@nestjs/common';

import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';

//Redis
import { RedisExperiyDurationEnum } from '@/infrastructure/redis/enums/redis.expiry-duration.enums';
import { RedisService } from '@/infrastructure/redis/redis.service';
import { AuthPayload } from '@Interfaces/auth.payload.interface';

import { applyDecorators, SetMetadata } from '@nestjs/common';

export const AuthNotRequired = (value: boolean) => {
    return applyDecorators(SetMetadata('AuthNotRequired', value));
};

@Injectable()
export class AuthJWTGuard implements CanActivate {
    constructor(
        private readonly lambdaService: LambdaService,
        private readonly reflector: Reflector,
        private jwtService: JwtService,
        protected readonly RedisService?: RedisService
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const authNotRequired = this.reflector.get<boolean>(
            'AuthNotRequired',
            context.getHandler()
        );

        //Pass the guard if we are in dev environment
        // if (process.env.NODE_ENV === 'dev') return true;

        //Pass the guard if it does not requiere an authentifcation
        if (authNotRequired) return true;

        const request = context.switchToHttp().getRequest();

        const token = this.extractTokenFromHeader(request);
        if (!token)
            throw new HttpException('No Token Provided', HttpStatus.FORBIDDEN);

        try {
            const payload: AuthPayload =
                await this.jwtService.verifyAsync(token);

            //User Id
            const userId = payload?.userId;
            const key = `${userId}:${payload.appId}`;
            if (!userId) throw Error();

            //Check Token In Cache
            const cacheToken = await this.RedisService.get('token', key);

            //Check User in Cache
            let user: any = JSON.parse(
                await this.RedisService.get('user', key)
            );

            //Check Token
            if (!cacheToken || !user) {
                const response = await this.lambdaService.callApi({
                    url: `${process.env.API_TOKEN_URL}/token/validate/${token}`,
                    method: 'POST',
                    payload: {},
                });
                //Cache Token
                await this.RedisService.setWithExpiry(
                    'token',
                    response.data.userId,
                    token,
                    RedisExperiyDurationEnum.ONE_HOUR
                );
                //Cache User
                // user = await this.userService.findById(userId);
                user = await this.lambdaService.callApi({
                    url: `${process.env.API_CORE_URL}/user/${userId}`,
                    method: 'GET',
                    token,
                });

                await this.RedisService.setWithExpiry(
                    'user',
                    userId.toString(),
                    JSON.stringify(user),
                    RedisExperiyDurationEnum.ONE_HOUR
                );
            }

            const { password, ...userWithoutPassword } = user;

            //Implement user id
            request.user = userWithoutPassword;

            //Super Admin
            let findSuperAdmin = false;
            if (user.roles) {
                findSuperAdmin = user?.roles.find(
                    (r) => r.publicId === 'super-admin'
                );
            }
            request.superAdmin = findSuperAdmin ? true : false;

            //Implement app id
            request.appId = payload?.appId;

            return true;
        } catch (e) {
            console.log('e: ', e);
            throw new UnauthorizedException();
        }
    }

    private extractTokenFromHeader(request: Request): string | undefined {
        const [type, token] = request.headers.authorization?.split(' ') ?? [];
        return type === 'Bearer' ? token : undefined;
    }
}
