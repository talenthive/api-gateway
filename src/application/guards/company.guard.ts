import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

import { SetMetadata } from '@nestjs/common';

export const CheckCompany = (...args: any[]) =>
    SetMetadata('CheckCompany', args);

@Injectable()
export class CompanyGuard extends AuthGuard('jwt') {
    constructor(private reflector: Reflector) {
        super();
    }
    async canActivate(context: ExecutionContext): Promise<any> {
        const restriction: boolean = this.reflector.get(
            'CheckCompany',
            context.getHandler()
        );

        //No Restriction
        if (!restriction) return true;

        const request = context.switchToHttp().getRequest();
        const params = request.params;
        const companyId = params.companyId;
        const user = request.user;
        const companies = user.companies;
        const superAdmin = request.superAdmin;

        //User is a super admin
        if (superAdmin) return true;

        //Exclusion
        if (!companies.includes(companyId)) return false;
        else return true;
    }
}
