import { Injectable, ExecutionContext, HttpStatus } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

import { SetMetadata } from '@nestjs/common';

export const OnlyMe = (...args: any[]) => SetMetadata('OnlyMe', args);

@Injectable()
export class OnlyMeGuard extends AuthGuard('jwt') {
    constructor(private reflector: Reflector) {
        super();
    }
    async canActivate(context: ExecutionContext): Promise<any> {
        const restriction: boolean = this.reflector.get(
            'OnlyMe',
            context.getHandler()
        );

        //No Restriction
        if (!restriction) return true;

        const request = context.switchToHttp().getRequest();
        const params = request.params;
        const userId = params.userId;
        const user = request.user;
        const superAdmin = context.switchToHttp().getRequest().superAdmin;

        //User is a super admin
        if (superAdmin) return true;

        //Exclusion
        if (user.id !== userId) return false;
        else return true;
    }
}
