import {
    ArgumentsHost,
    BadRequestException,
    Catch,
    ExceptionFilter,
    HttpException,
    HttpStatus,
    Inject,
    Logger,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { PrismaClientValidationError } from '@prisma/client/runtime/library';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
    constructor(
        private readonly httpAdapterHost: HttpAdapterHost,
        @Inject()
        private readonly logger: Logger
    ) {}

    catch(exception: Error, host: ArgumentsHost): void {
        const { httpAdapter } = this.httpAdapterHost;

        const ctx = host.switchToHttp();

        let httpStatus: number;
        let message: string;
        let details: any;

        //HTTP Exception
        if (exception instanceof HttpException) {
            const response = exception.getResponse();
            httpStatus = exception.getStatus();
            message = exception.message;
            details = response['message'];
        }
        //Bad Request
        else if (exception instanceof BadRequestException) {
            message = exception.message;
            details = exception.getResponse();
        }

        //Prisma Validation
        else if (exception instanceof PrismaClientValidationError) {
            const errorObject = this.parsePrismaError(exception);
            message = errorObject.message;
        }
        //Others
        else {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            message = 'Something wrong happened';
        }

        const responseBody = {
            statusCode: httpStatus,
            timestamp: new Date().toISOString(),
            message,
            details,
            route: httpAdapter.getRequestUrl(ctx.getRequest()),
        };

        //Send Error
        httpAdapter.reply(ctx.getResponse(), responseBody, httpStatus);

        //Log
        this.logger.error(message, responseBody);
    }

    private parsePrismaError(error: PrismaClientValidationError): {
        type: string;
        message: string;
        filePath: string;
        line: number;
        context: string;
    } {
        const errorString = error.message;
        const lines = errorString.split('\n');
        const type = lines[0].split(':')[0];
        const message = lines[1].trim();
        const filePath = lines[2].trim();
        const lineMatch = lines[2].match(/:(\d+):\d+/);
        const line = lineMatch ? parseInt(lineMatch[1], 10) : null;
        const context = lines.slice(3).join('\n').trim();

        return {
            type,
            message,
            filePath,
            line,
            context,
        };
    }
}
