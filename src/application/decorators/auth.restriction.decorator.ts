import { applyDecorators, SetMetadata } from '@nestjs/common';

export const AuthNotRequired = (value: boolean) => {
    return applyDecorators(SetMetadata('AuthNotRequired', value));
};
