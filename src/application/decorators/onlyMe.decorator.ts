import { SetMetadata } from '@nestjs/common';

export const OnlyMe = (OnlyMe: boolean) => SetMetadata('OnlyMe', OnlyMe);
