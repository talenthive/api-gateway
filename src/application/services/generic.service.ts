//NestJs
import { Injectable } from '@nestjs/common';

//Prisma
import { PrismaClient } from '@prisma/client';

//Services
import { ElasticSearchService } from '@/infrastructure/elastic-search/elastic-search.service';
import { PrismaService } from '@Prisma/prisma.service';

interface SchemaProperties {
    id: string;
}

@Injectable()
export abstract class GenericService<
    Schema extends SchemaProperties,
    SchemaDelegate,
> {
    protected prisma: PrismaClient;
    protected repository: SchemaDelegate;
    protected useCache: boolean = false;

    constructor(
        protected readonly schema: string,
        protected readonly prismaService: PrismaService,
        protected readonly elasticSearchService: ElasticSearchService
    ) {
        if (!schema) throw Error('Schemá is not defined');
        this.prisma = new PrismaClient();
        this.repository = this.prisma[schema];
    }

    //Create Element
    async create(data: any): Promise<Schema> {
        const element: Schema = await this.prisma[this.schema].create({
            data,
        });
        return element;
    }

    //Create Many Elements
    async createMany(data: any[]): Promise<Schema[]> {
        const elements: Schema[] = await this.prisma[this.schema].createMany({
            data,
        });
        return elements;
    }

    //Find Elements
    async find<T>(params: T): Promise<Schema[]> {
        const elements = await this.prismaService[this.schema].findMany({
            where: params,
        });
        return elements;
    }

    //Find Single Element
    async findById(id: string): Promise<Schema> {
        let element: Schema;

        element = <Schema>await this.prismaService[this.schema].findUnique({
            where: {
                id,
            },
        });

        return element;
    }

    //Find First Element
    async findFirstElement(): Promise<Schema> {
        const elements = <Schema[]>await this.prismaService[
            this.schema
        ].findMany({
            orderBy: {
                id: 'asc',
            },
            take: 1,
        });
        return elements[0] ? elements[0] : null;
    }

    //Find Last Element
    async findLastElement(): Promise<Schema> {
        const elements = <Schema[]>await this.prismaService[
            this.schema
        ].findMany({
            orderBy: {
                id: 'desc',
            },
            take: 1,
        });
        return elements[0] ? elements[0] : null;
    }

    //Find By Ids
    async findByIds(ids: string[]): Promise<Schema[]> {
        const elements = await this.prismaService[this.schema].findMany({
            where: { id: { in: ids } },
        });
        return elements;
    }

    //Update Element
    async update(id: string, body: any): Promise<Schema> {
        const existingElement = this.findById(id);
        const updatedElement = { ...existingElement, ...body };
        const result = <Schema>await this.prismaService[this.schema].update({
            where: {
                id,
            },
            data: updatedElement,
        });
        return result;
    }

    //Remove Element
    async delete(id: string): Promise<Boolean> {
        const result = await this.prismaService[this.schema].delete({
            where: {
                id,
            },
        });
        return true;
    }

    //Count Element
    async count(where: any): Promise<Number> {
        const result = await this.prismaService[this.schema].count({
            where,
        });
        return result;
    }

    //Search Elements
    async search(text: string, fields: string[]): Promise<Schema[]> {
        const results = await this.elasticSearchService.search<Schema>(
            this.schema,
            {
                query: {
                    multi_match: {
                        query: text,
                        fields: fields,
                    },
                },
            }
        );
        const ids: string[] = results.map((result) => result.id);
        if (!ids.length) {
            return [];
        }

        const result = await this.findByIds(ids);
        return result;
    }
}
