export interface ApiResponse {
    success: boolean;
    statusCode: number;
    status: string;
    data?: any;
}
