export interface ApiRequest {
    url: string;
    method: 'POST' | 'GET' | 'UPDATE' | 'PUT' | 'PATCH' | 'DELETE';
    token?: string;
    payload?: any;
}
