// ESM
import { faker } from '@faker-js/faker';

//Entity
import { Prisma } from '@prisma/client';

//DTO
import { ArticleCreateInput } from '@/modules/article/dto/article.create.input';

export const createFakeArticle = (): ArticleCreateInput => {
    return {
        title: faker.word.words(5),
        content: faker.lorem.text(),
    };
};

export const generateFakeArticles = (
    count = 5
): Prisma.ArticleCreateInput[] => {
    return faker.helpers.multiple(createFakeArticle, {
        count,
    });
};
