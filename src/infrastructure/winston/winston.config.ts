import { transports, format } from 'winston';
import * as winston from 'winston';
import { utilities as nestWinstonModuleUtilities } from 'nest-winston';
import 'winston-daily-rotate-file';

const logFormat = winston.format.printf(
    ({ timestamp, level, message, context }) => {
        return `${timestamp} [${context}] ${level}: ${message}`;
    }
);

// Add colors for log levels
winston.addColors({
    error: 'red',
    warn: 'yellow',
    info: 'green',
    debug: 'blue',
    verbose: 'cyan',
});

export const winstonConfig = {
    transports: [
        // file on daily rotation (error only)
        new transports.DailyRotateFile({
            // %DATE will be replaced by the current date
            filename: `logs/%DATE%-error.log`,
            level: 'error',
            // format: format.combine(format.timestamp(), format.json()),
            format: winston.format.combine(
                winston.format.timestamp(),
                logFormat
            ),
            datePattern: 'YYYY-MM-DD',
            zippedArchive: false, // don't want to zip our logs
            maxFiles: '30d', // will keep log until they are older than 30 days
        }),
        // same for all levels
        // new transports.DailyRotateFile({
        //     filename: `logs/%DATE%-combined.log`,
        //     format: format.combine(format.timestamp(), format.json()),
        //     datePattern: 'YYYY-MM-DD',
        //     zippedArchive: false,
        //     maxFiles: '30d',
        // }),
        new transports.Console({
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.ms(),
                nestWinstonModuleUtilities.format.nestLike('TalentHive', {
                    colors: true,
                    prettyPrint: true,
                    processId: true,
                    appName: true,
                })
            ),
        }),
    ],
};
