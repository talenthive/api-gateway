import { Global, Module } from '@nestjs/common';

//Service
import { PrismaService } from './prisma.service';

//Modules
@Global()
@Module({
    imports: [],
    exports: [PrismaService],
    providers: [PrismaService],
})
export class PrismaModule {}
