import { generateFakeArticles } from '@Infrastructure/faker/article.faker';
import { Prisma } from '@prisma/client';

//Prisma Connect
import prisma from '@Prisma/prisma.connect';

async function main() {
    const articles: Prisma.ArticleCreateInput[] = generateFakeArticles();

    const addArticles = async () =>
        await prisma.article.createMany({ data: articles });

    console.log('Add Articles: ', articles);

    addArticles();
}

main()
    .catch((e) => {
        console.error(e);
        process.exit(1);
    })
    .finally(async () => {
        await prisma.$disconnect();
    });
