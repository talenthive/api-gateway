import { PrismaService } from '../prisma.service';

describe('PrismaService', () => {
    let prismaService: PrismaService;

    beforeAll(async () => {
        prismaService = new PrismaService();
        await prismaService.connect();
    });

    afterAll(async () => {
        await prismaService.disconnect();
    });

    test('should connect to the database', async () => {
        await expect(prismaService.connect()).resolves.not.toThrow();
    });

    test('should disconnect from the database', async () => {
        await prismaService.connect(); // Ensure connection before testing disconnection
        await expect(prismaService.disconnect()).resolves.not.toThrow();
    });
});
