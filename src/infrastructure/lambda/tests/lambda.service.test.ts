//NestJs
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';

//Lambda
import { LambdaService } from '../lambda.service';

describe('LambdaService', () => {
    let service: LambdaService;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [ConfigModule.forRoot()],
            providers: [LambdaService, ConfigService],
        }).compile();
        //Services
        service = module.get<LambdaService>(LambdaService);
    });

    it('Service should be defined', () => {
        expect(service).toBeDefined();
    });

    it('Service should be able to say hello via the invoke Api', async () => {
        const response = await service.callApi({
            url: `${process.env.API_TOKEN_URL}/hello`,
            method: 'GET',
        });
        expect(response).toBeDefined();
        expect(response.statusCode).toBe(200);
        expect(response.data).toBe('Hello World!');
    });
});
