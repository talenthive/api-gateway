import { Module } from '@nestjs/common';

//Service
import { LambdaService } from './lambda.service';

//Modules

@Module({
    imports: [],
    exports: [LambdaService],
    controllers: [],
    providers: [LambdaService],
})
export class LambdaModule {}
