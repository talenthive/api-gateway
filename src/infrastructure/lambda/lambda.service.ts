import { Injectable } from '@nestjs/common';

//Interfaces
import { ApiRequest } from '@/application/interfaces/api.request.interface';
import { ApiResponse } from '@Application/interfaces/api.response.interface';

@Injectable()
export class LambdaService {
    async callApi(params: ApiRequest): Promise<ApiResponse> {
        try {
            const { method, url, payload, token } = params;

            const json = {
                method,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: token ? `Bearer ${token}` : '',
                },
            };

            //Body
            if (method === 'POST') {
                Object.assign(json, { body: JSON.stringify(payload) });
            }

            const response = await fetch(url, json);
            if (!response.ok) {
                const errorData = await response.json();
                throw new Error(`Error: ${errorData.message}`);
            }
            return response.json();
        } catch (error) {
            throw new Error(`Error invoking API Rest: ${error}`);
        }
    }
}
