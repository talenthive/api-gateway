import { FactoryProvider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Redis } from 'ioredis';

export const RedisFactory: FactoryProvider<Redis> = {
    provide: 'RedisClient',
    useFactory: async (configService: ConfigService) => {
        const redisInstance = new Redis({
            host: configService.get<string>('REDIS_HOST'),
            port: +configService.get<number>('REDIS_PORT'),
        });

        redisInstance.on('error', (e) => {
            throw new Error(`Redis connection failed: ${e}`);
        });

        return redisInstance;
    },
    inject: [ConfigService],
};
