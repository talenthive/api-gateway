import { Global, Module } from '@nestjs/common';

import { RedisFactory } from './redis.factory';
import { RedisService } from './redis.service';
@Global()
@Module({
    imports: [],
    controllers: [],
    providers: [RedisFactory, RedisService],
    exports: [RedisService],
})
export class RedisModule {}
