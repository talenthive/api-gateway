import { Inject, Injectable, OnModuleDestroy } from '@nestjs/common';
import { Redis } from 'ioredis';

import { RedisServiceInterface } from './interfaces/redis.repository.interface';

@Injectable()
export class RedisService implements OnModuleDestroy, RedisServiceInterface {
    constructor(@Inject('RedisClient') private readonly redisClient: Redis) {}

    async onModuleDestroy(): Promise<void> {
        this.redisClient.disconnect();
    }

    async get(key: string, sufix: string): Promise<string | null> {
        return await this.redisClient.get(`${key}:${sufix}`);
    }

    async set(key: string, sufix: string, value: string): Promise<void> {
        await this.redisClient.set(`${key}:${sufix}`, value);
    }

    async delete(key: string, sufix: string): Promise<void> {
        await this.redisClient.del(`${key}:${sufix}`);
    }

    async increment(key: string, suffix: string): Promise<number> {
        const existingKey = this.get(key, suffix);
        if (existingKey) {
            return await this.redisClient.incr(`${key}:${suffix}`);
        } else {
            await this.set(key, suffix, '1');
            return parseInt(await this.get(key, suffix));
        }
    }

    async setWithExpiry(
        key: string,
        sufix: string,
        value: string,
        expiry: number
    ): Promise<void> {
        await this.redisClient.set(`${key}:${sufix}`, value, 'EX', expiry);
    }
}
