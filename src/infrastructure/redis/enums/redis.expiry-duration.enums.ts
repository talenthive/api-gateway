export enum RedisExperiyDurationEnum {
    ONE_MINUTE = 60,
    ONE_HOUR = 3600,
    ONE_DAY = 86400,
}
