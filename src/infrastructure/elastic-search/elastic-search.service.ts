import {
    SearchHit,
    SearchResponse,
} from '@elastic/elasticsearch/lib/api/types';
import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';

@Injectable()
export class ElasticSearchService {
    constructor(private readonly elasticSearchService: ElasticsearchService) {}

    async search<R>(index: string, query: any): Promise<R[]> {
        //Check if index exists
        await this.checkIndexExists(index);

        const result: SearchResponse<R> =
            await this.elasticSearchService.search({
                index,
                body: query,
            });
        const hits: SearchHit<R>[] = result.hits.hits;
        const documents: R[] = hits.map((item) => item._source as R);
        return documents;
    }

    async checkIndexExists(index: string) {
        const exists = await this.elasticSearchService.indices.exists({
            index,
        });
        if (!exists) {
            await this.elasticSearchService.indices.create({ index });
        }
    }

    async index<T>(index: string, id: string, body: T): Promise<boolean> {
        this.elasticSearchService.index({
            index,
            id,
            body,
        });
        return true;
    }

    async delete(index: string, id: string): Promise<Boolean> {
        this.elasticSearchService.delete({
            index,
            id,
        });
        return true;
    }
}
