import { Controller, Get, Param, Query } from '@nestjs/common';
import { ElasticSearchService } from './elastic-search.service';

@Controller('search')
export class SearchController {
    constructor(private readonly elasticSearchService: ElasticSearchService) {}

    @Get()
    async search(@Param('index') index: string, @Query('q') query: string) {
        const results = await this.elasticSearchService.search(index, {
            query: {
                match: { field: query },
            },
        });
        return results;
    }
}
