import { RedisService } from '../src/infrastructure/redis/redis.service';
import { Redis } from 'ioredis';

const mockRedisClient = {
    get: () => Promise.resolve(null),
    set: () => Promise.resolve(),
    del: () => Promise.resolve(),
    disconnect: () => Promise.resolve(),
} as unknown as Redis;

module.exports = async () => {};
