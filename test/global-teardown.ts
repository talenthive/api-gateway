// global-teardown.js
import { RedisService } from '../src/infrastructure/redis/redis.service';
import { PrismaService } from '../src/infrastructure/prisma/prisma.service';
import Redis from 'ioredis';
const mockRedisClient = {
    get: () => Promise.resolve(null),
    set: () => Promise.resolve(),
    del: () => Promise.resolve(),
    disconnect: () => Promise.resolve(),
} as unknown as Redis;

module.exports = async () => {
    console.log('Global teardown: Tests Ended!');
    // Prisma
    const prismaService = new PrismaService();
    await prismaService.onModuleDestroy();

    // Redis
    const redisService = new RedisService(mockRedisClient);
    await redisService.onModuleDestroy();
};
